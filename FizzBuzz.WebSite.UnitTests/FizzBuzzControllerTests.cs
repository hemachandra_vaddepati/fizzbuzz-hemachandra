﻿// <copyright file="FizzBuzzControllerTests.cs" company="TCS">
// Copyright (C) TCS. All rights reserved. 
// </copyright>

using System;
using System.Collections.Generic;
using System.Web.Mvc;
using FizzBuzz.Service;
using FizzBuzz.WebSite.Controllers;
using FizzBuzz.WebSite.Models;
using Moq;
using NUnit.Framework;

namespace FizzBuzz.WebSite.UnitTests
{
    /// <summary>
    /// Class to test FizzBuzzController
    /// </summary>
    [TestFixture()]
    public class FizzBuzzControllerTests
    {
        /// <summary>
        /// VAriable holding mock for IFizzBuzzService
        /// </summary>
        private Mock<IFizzBuzzService> fizzBuzzServiceMock;

        /// <summary>
        /// Moq setup
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            fizzBuzzServiceMock = new Mock<IFizzBuzzService>();
        }

        /// <summary>
        /// Test: On loading website, Index view should be called by default
        /// </summary>
        [Test]
        public void WhenApplicationLoads_Index_ShouldRender_IndexView()
        {

            var controller = new FizzBuzzController(fizzBuzzServiceMock.Object);
            ViewResult result = (ViewResult)controller.Index(null, null);
            Assert.NotNull(result);
            Assert.That(result.ViewName, Is.EqualTo("Index"));
        }

        /// <summary>
        /// Test: When user enteredNumber and submits, GetData should render Index view with FizzBuzzList
        /// </summary>
        [Test]
        public void WhenUserEnteredNumberAndSubmits_Index_ShouldRender_IndexViewWithFizzBuzzList()
        {
            string userInput = "10";

            List<string> expectedList = new List<string>()
            {
                "1","2","fizz","4","buzz","fizz","7","8","fizz","buzz"
            };

            var controller = new FizzBuzzController(fizzBuzzServiceMock.Object);
            fizzBuzzServiceMock.Setup(x => x.GetFizzBuzzData(Convert.ToInt32(userInput))).Returns(expectedList);
            var result = controller.Index(userInput, 1) as ViewResult;

            Assert.IsNotNull(result);
            var data = result.Model as FizzBuzzModel;

            Assert.IsNotNull(data);
            Assert.AreEqual(Convert.ToInt32(userInput), data.FizzBuzzList.Count);


        }
    }
}
