﻿// <copyright file="DivisibleByFiveNumeralHandler.cs" company="TCS">
// Copyright (C) TCS. All rights reserved. 
// </copyright>

using System;

namespace FizzBuzz.Service.Handlers
{
    /// <summary>
    /// Class that handles rules for all numbers which are divisible by 5
    /// </summary>
    public class DivisibleByFiveNumeralHandler : INumeralHandler
    {
        /// <summary>
        /// Variable holding class for dayofweek
        /// </summary>
        private readonly IDayOfWeekHandler dayOfWeekHandler;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dayOfWeekHandler">Indicating class for dayofweek</param>
        public DivisibleByFiveNumeralHandler(IDayOfWeekHandler dayOfWeekHandler)
        {
            this.dayOfWeekHandler = dayOfWeekHandler;
        }

        /// <summary>
        /// Checks whether this class has rules for specific number
        /// </summary>
        /// <param name="number"></param>
        /// <returns>Returns true if it handles specific number or else false</returns>
        public bool CanHandle(int number)
        {
            return number % 5 == 0;
        }

        /// <summary>
        /// Gets new value based on rules
        /// </summary>
        /// <returns>Returns value based on rules</returns>
        public string GetResult()
        {
         return dayOfWeekHandler.CanHandle(DateTime.Now.DayOfWeek)? "wuzz": "buzz";
        }
    }
}
