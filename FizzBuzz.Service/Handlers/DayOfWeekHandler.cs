﻿// <copyright file="DayOfWeekHandler.cs" company="TCS">
// Copyright (C) TCS. All rights reserved. 
// </copyright>

using System;

namespace FizzBuzz.Service.Handlers
{    
    /// <summary>
    /// Class that handles rules for dayofweek
    /// </summary>
    public class DayOfWeekHandler : IDayOfWeekHandler
    {
        /// <summary>
        /// Checks whether this class has rules for specific day of week
        /// </summary>
        /// <param name="dayOfWeek">Indicates specific day of week</param>
        /// <returns>Returns true if it handles specific day of week or else false</returns>
        public bool CanHandle(DayOfWeek dayOfWeek)
        {
            return dayOfWeek == DayOfWeek.Wednesday;
        }
    }
}
