﻿// <copyright file="DivisibleByThreeNumeralHandler.cs" company="TCS">
// Copyright (C) TCS. All rights reserved. 
// </copyright>

using System;

namespace FizzBuzz.Service.Handlers
{
    /// <summary>
    /// Class that handles rules for all numbers which are divisible by 3
    /// </summary>
    public class DivisibleByThreeNumeralHandler : INumeralHandler
    {
        /// <summary>
        /// Variable holding class for dayofweek
        /// </summary>
        private readonly IDayOfWeekHandler dayOfWeekHandler;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dayOfWeekHandler">Indicating class for dayofweek</param>
        public DivisibleByThreeNumeralHandler(IDayOfWeekHandler dayOfWeekHandler)
        {
            this.dayOfWeekHandler = dayOfWeekHandler;
        }

        /// <summary>
        /// Checks whether this class has rules for specific number
        /// </summary>
        /// <param name="number"></param>
        /// <returns>Returns true if it handles specific number or else false</returns>
        public bool CanHandle(int number)
        {
            return number % 3 == 0;
        }

        /// <summary>
        /// Gets new value based on rules
        /// </summary>
        /// <returns>Returns value based on rules</returns>
        public string GetResult()
        {
            return dayOfWeekHandler.CanHandle(DateTime.Now.DayOfWeek) ? "wizz" : "fizz";
        }
    }
}
