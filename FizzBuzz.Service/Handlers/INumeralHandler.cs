﻿// <copyright file="INumeralHandler.cs" company="TCS">
// Copyright (C) TCS. All rights reserved. 
// </copyright>

namespace FizzBuzz.Service.Handlers
{    /// <summary>
     /// Interface that handles rules for all numbers 
     /// </summary>
    public interface INumeralHandler
    {
        /// <summary>
        ///check whether it will handle request
        /// </summary>
        bool CanHandle(int number);

        /// <summary>
        /// Gets new value based on rules
        /// </summary>
        /// <returns>Returns value based on rules</returns>
        string GetResult();
    }
}