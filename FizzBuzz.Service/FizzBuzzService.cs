﻿// <copyright file="FizzBuzzService.cs" company="TCS">
// Copyright (C) TCS. All rights reserved. 
// </copyright>

using System.Collections.Generic;
using System.Linq;
using System.Text;
using FizzBuzz.Service.Handlers;

namespace FizzBuzz.Service
{
    /// <summary>
    /// Class holds custom 'fizz-buzz' logic to generate list of words
    /// </summary>
    public class FizzBuzzService : IFizzBuzzService
    {
        /// <summary>
        /// Variable holding all objects of numeralHandlers
        /// </summary>
        private readonly IEnumerable<INumeralHandler> numeralHandlers;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="numeralHandlers">Indicating all objects of numeralHandlers</param>
        public FizzBuzzService(IEnumerable<INumeralHandler> numeralHandlers)
        {
            this.numeralHandlers = numeralHandlers;
        }

        /// <summary>
        /// Returns fizz buzz list of words
        /// </summary>
        /// <param name="userEnteredValue">Higher number prompted by user</param>
        /// <returns>List of words in string formats</returns>
        public List<string> GetFizzBuzzData(int userEnteredValue)
        {
            var fizzBuzzList = new List<string>();

            for (var numberIndex = 1; numberIndex <= userEnteredValue; numberIndex++)
            {
                var matchedNumeralHandlers = numeralHandlers.Where(x => x.CanHandle(numberIndex)).ToList();
                if (matchedNumeralHandlers.Any())
                {
                    var result = string.Join(" ", matchedNumeralHandlers.Select(x => x.GetResult()));                 
                    fizzBuzzList.Add(result);
                }
                else
                {
                    fizzBuzzList.Add(numberIndex.ToString());
                }
            }
            return fizzBuzzList;
        }
    }

}
