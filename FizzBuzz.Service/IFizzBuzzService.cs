﻿// <copyright file="IFizzBuzzService.cs" company="TCS">
// Copyright (C) TCS. All rights reserved. 
// </copyright>

using System.Collections.Generic;

namespace FizzBuzz.Service
{   
    /// <summary>
    /// Interface holds custom 'fizz-buzz' logic to generate list of words
    /// </summary>
    public interface IFizzBuzzService
    {
        /// <summary>
        /// Returns list of words
        /// </summary>
        /// <param name="userEnteredValue">Higher number prompted by user</param>
        /// <returns>List of numbers in string</returns>
        List<string> GetFizzBuzzData(int userEnteredValue);
    }
}
