﻿// <copyright file="FizzBuzzServiceTests.cs" company="TCS">
// Copyright (C) TCS. All rights reserved. 
// </copyright>

using System.Collections.Generic;
using FizzBuzz.Service.Handlers;
using Moq;
using NUnit.Framework;

namespace FizzBuzz.Service.UnitTests
{
    /// <summary>
    /// Class to test FizzBuzzService
    /// </summary>
    [TestFixture()]
    class FizzBuzzServiceTests
    {
        /// <summary>
        /// Variable holding mock for INumeralHandler
        /// </summary>
        private Mock<INumeralHandler> divisibleByThreeNumeralHandlerMock;

        /// <summary>
        /// Variable holding mock for INumeralHandler
        /// </summary>
        private Mock<INumeralHandler> divisibleByFiveNumeralHandlerMock;

        /// <summary>
        /// Variable holding mock for FizzBuzzService
        /// </summary>
        private FizzBuzzService fizzBuzzService;

        /// <summary>
        /// Moq setup method which runs before each test
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            divisibleByThreeNumeralHandlerMock = new Mock<INumeralHandler>();
            divisibleByThreeNumeralHandlerMock.Setup(x => x.CanHandle(It.Is<int>(i => i % 3 == 0))).Returns(true);
            
            divisibleByFiveNumeralHandlerMock = new Mock<INumeralHandler>();
            divisibleByFiveNumeralHandlerMock.Setup(x => x.CanHandle(It.Is<int>(i => i % 5 == 0))).Returns(true);

            fizzBuzzService = new FizzBuzzService(new[]
            {
                divisibleByFiveNumeralHandlerMock.Object,
                divisibleByThreeNumeralHandlerMock.Object
            });
        }

        /// <summary>
        /// Test: When today is wednesday and user input is 10, GetFizzBuzzData should return list from 1To10 and contains wizz wuzz
        /// </summary>
        [Test]
        public void GivenANumberAndTodayIsWednesday_GetFizzBuzzData_ShouldReturn_ListWithWizzWuzzFromOneToNumber()
        {
            //setup
            var expectedListIfWednesday = new List<string>()
            {
                "1","2","wizz","4","wuzz","wizz","7","8","wizz","wuzz"
            };
            divisibleByThreeNumeralHandlerMock.Setup(x => x.GetResult()).Returns("wizz");
            divisibleByFiveNumeralHandlerMock.Setup(x => x.GetResult()).Returns("wuzz");

            //Execute method
            var result = fizzBuzzService.GetFizzBuzzData(10);

            //Assertion
            Assert.AreEqual(10, result.Count);
            CollectionAssert.AreEqual(expectedListIfWednesday, result);
            
        }

        /// <summary>
        /// Test: When today is not wednesday and user input is 10, GetFizzBuzzData should return list from 1To10 and contains fizz buzz
        /// </summary>
        [Test]
        public void GivenANumberAndTodayIsNotWednesday_GetFizzBuzzData_ShouldReturn_ListWithFizzBuzzFromOneToNumber()
        {
            //setup
            var expectedListIfNotWednesday = new List<string>()
            {
                "1","2","fizz","4","buzz","fizz","7","8","fizz","buzz"
            };
            divisibleByThreeNumeralHandlerMock.Setup(x => x.GetResult()).Returns("fizz");
            divisibleByFiveNumeralHandlerMock.Setup(x => x.GetResult()).Returns("buzz");
            
            //Execute method
            var result = fizzBuzzService.GetFizzBuzzData(10);

            //Assertion
            Assert.AreEqual(10, result.Count);
            CollectionAssert.AreEqual(expectedListIfNotWednesday, result);
            

        }
    }
}
