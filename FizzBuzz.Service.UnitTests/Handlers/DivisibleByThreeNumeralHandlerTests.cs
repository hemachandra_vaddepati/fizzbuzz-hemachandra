﻿// <copyright file="DivisibleByThreeNumeralHandlerTests.cs" company="TCS">
// Copyright (C) TCS. All rights reserved. 
// </copyright>

using System;
using FizzBuzz.Service.Handlers;
using Moq;
using NUnit.Framework;

namespace FizzBuzz.Service.UnitTests.Handlers
{
    /// <summary>
    /// Class which tests DivisibleByThreeNumeralHandler
    /// </summary>
    [TestFixture]
    public class DivisibleByThreeNumeralHandlerTests
    {
        /// <summary>
        /// Variable holding mock for IDayOfWeekHandler
        /// </summary>
        private Mock<IDayOfWeekHandler> dayOfWeekHandlerMock;

        /// <summary>
        /// Moq setup method which runs before each test
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            dayOfWeekHandlerMock = new Mock<IDayOfWeekHandler>();
        }

        /// <summary>
        /// Test: Should handle all numbers divisible by 3
        /// </summary>
        [Test]
        public void WhenTheNumber_IsDivisibleByThree_CanHandle_Should_ReturnTrue()
        {
            var divisibleByThree = new DivisibleByThreeNumeralHandler(dayOfWeekHandlerMock.Object);
            Assert.AreEqual(true, divisibleByThree.CanHandle(9));
        }

        /// <summary>
        /// Test: Should not handle all numbers not divisible by 3
        /// </summary>
        [Test]
        public void WhenTheNumber_IsNotDivisibleByThree_CanHandle_Should_ReturnFalse()
        {
            var divisibleByThree = new DivisibleByThreeNumeralHandler(dayOfWeekHandlerMock.Object);
            Assert.AreEqual(false, divisibleByThree.CanHandle(7));
        }

        /// <summary>
        /// Test: When today is  wednesday, GetResult() method should return wizz
        /// </summary>
        [Test]
        public void WhenTodayIsWednesday_GetResult_Should_ReturnWizz()
        {
            var divisibleByThree = new DivisibleByThreeNumeralHandler(dayOfWeekHandlerMock.Object);
            dayOfWeekHandlerMock.Setup(x => x.CanHandle(It.IsAny<DayOfWeek>())).Returns(true);

            Assert.AreEqual("wizz", divisibleByThree.GetResult());
        }

        /// <summary>
        /// Test: When today is not wednesday, GetResult() method should return Fizz
        /// </summary>
        [Test]
        public void WhenTodayIsNotWednesday_GetResult_Should_ReturnFizz()
        {
            var divisibleByThree = new DivisibleByThreeNumeralHandler(dayOfWeekHandlerMock.Object);
            dayOfWeekHandlerMock.Setup(x => x.CanHandle(It.IsAny<DayOfWeek>())).Returns(false);

            Assert.AreEqual("fizz", divisibleByThree.GetResult());
        }

    }
}
