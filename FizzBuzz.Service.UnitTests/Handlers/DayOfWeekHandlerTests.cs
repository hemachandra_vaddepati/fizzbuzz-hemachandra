﻿// <copyright file="DayOfWeekHandlerTests.cs" company="TCS">
// Copyright (C) TCS. All rights reserved. 
// </copyright>

using System;
using FizzBuzz.Service.Handlers;
using NUnit.Framework;

namespace FizzBuzz.Service.UnitTests.Handlers
{
    /// <summary>
    /// Class to test DayOfWeekHandler
    /// </summary>
    [TestFixture()]
    class DayOfWeekHandlerTests
    {
        private DayOfWeekHandler dayOfWeekHandler;

        /// <summary>
        /// Moq setup method which runs before each test
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            dayOfWeekHandler = new DayOfWeekHandler();
        }

        /// <summary>
        /// Test: CanHandle returns true if today is wednesday
        /// </summary>
        [Test]
        public void WhenTodayIsWednesday_CanHandle_Should_ReturnTrue()
        {
            Assert.AreEqual(true, dayOfWeekHandler.CanHandle(DayOfWeek.Wednesday));
        }

        /// <summary>
        /// Test: CanHandle returns false if today is not wednesday
        /// </summary>
        [Test]
        public void WhenTodayisNotWednesday_CanHandle_Should_ReturnFalse()
        {
            Assert.AreEqual(false, dayOfWeekHandler.CanHandle(DayOfWeek.Monday));
        }
    }
}
