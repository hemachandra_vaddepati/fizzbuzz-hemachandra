﻿// <copyright file="DivisibleByFiveNumeralHandlerTests.cs" company="TCS">
// Copyright (C) TCS. All rights reserved. 
// </copyright>

using System;
using FizzBuzz.Service.Handlers;
using Moq;
using NUnit.Framework;

namespace FizzBuzz.Service.UnitTests.Handlers
{
    /// <summary>
    /// Class which tests DivisibleByFiveNumeralHandler
    /// </summary>
    [TestFixture]
    public class DivisibleByFiveNumeralHandlerTests
    {
        /// <summary>
        /// Variable holding mock for IDayOfWeekHandler
        /// </summary>
        private Mock<IDayOfWeekHandler> dayOfWeekHandlerMock;

        /// <summary>
        /// Moq setup method which runs before each test
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            dayOfWeekHandlerMock = new Mock<IDayOfWeekHandler>();
        }

        /// <summary>
        /// Test: Should handle all numbers divisible by 5
        /// </summary>
        [Test]
        public void WhenTheNumber_IsDivisibleByFive_CanHandle_Should_ReturnTrue()
        {
            var divisibleByFive = new DivisibleByFiveNumeralHandler(dayOfWeekHandlerMock.Object);
            Assert.AreEqual(true, divisibleByFive.CanHandle(10));
        }

        /// <summary>
        /// Test: Should not handle all numbers not divisible by 5
        /// </summary>
        [Test]
        public void WhenTheNumber_IsNotDivisibleByFive_CanHandle_Should_ReturnFalse()
        {
            var divisibleByFive = new DivisibleByFiveNumeralHandler(dayOfWeekHandlerMock.Object);
            Assert.AreEqual(false, divisibleByFive.CanHandle(7));
        }

        /// <summary>
        /// Test: When today is wednesday, GetResult() method should return wuzz
        /// </summary>
        [Test]
        public void WhenTodayIsWednesday_GetResult_Should_ReturnWuzz()
        {
            var divisibleByFive = new DivisibleByFiveNumeralHandler(dayOfWeekHandlerMock.Object);
            dayOfWeekHandlerMock.Setup(x => x.CanHandle(It.IsAny<DayOfWeek>())).Returns(true);

            Assert.AreEqual( "wuzz", divisibleByFive.GetResult());
        }

        /// <summary>
        /// Test: When today is not wednesday, GetResult() method should return buzz
        /// </summary>
        [Test]
        public void WhenTodayIsNotWednesday_GetResult_Should_ReturnBuzz()
        {
            var divisibleByFive = new DivisibleByFiveNumeralHandler(dayOfWeekHandlerMock.Object);
            dayOfWeekHandlerMock.Setup(x => x.CanHandle(It.IsAny<DayOfWeek>())).Returns(false);

            Assert.AreEqual("buzz", divisibleByFive.GetResult());
        }

    }
}
