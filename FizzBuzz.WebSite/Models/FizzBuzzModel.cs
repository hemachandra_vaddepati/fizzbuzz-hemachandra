﻿// <copyright file="FizzBuzzModel.cs" company="TCS">
// Copyright (C) TCS. All rights reserved. 
// </copyright>

using System.ComponentModel.DataAnnotations;

namespace FizzBuzz.WebSite.Models
{
    public class FizzBuzzModel
    {
        [Required(ErrorMessage = "Please enter a number")]
        [Range(1, 1000, ErrorMessage = "Please enter a valid number between 1 and 1000")]
        public int UserInput { get; set; }

        public PagedList.IPagedList<string> FizzBuzzList { get; set; }
    }
}